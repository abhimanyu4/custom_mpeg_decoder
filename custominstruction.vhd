--Copyright (C) 1991-2004 Altera Corporation
--Any megafunction design, and related net list (encrypted or decrypted),
--support information, device programming or simulation file, and any other
--associated documentation or information provided by Altera or a partner
--under Altera's Megafunction Partnership Program may be used only to
--program PLD devices (but not masked PLD devices) from Altera.  Any other
--use of such megafunction design, net list, support information, device
--programming or simulation file, or any other related documentation or
--information is prohibited for any other purpose, including, but not
--limited to modification, reverse engineering, de-compiling, or use with
--any other silicon devices, unless such use is explicitly licensed under
--a separate agreement with Altera or a megafunction partner.  Title to
--the intellectual property, including patents, copyrights, trademarks,
--trade secrets, or maskworks, embodied in any such megafunction design,
--net list, support information, device programming or simulation file, or
--any other related documentation or information provided by Altera or a
--megafunction partner, remains with Altera, the megafunction partner, or
--their respective licensors.  No other licenses, including any licenses
--needed under any third party's intellectual property, are provided herein.
--Copying or modifying any file, or portion thereof, to which this notice
--is attached violates this copyright.


-- VHDL Custom Instruction Template File for Extended Logic

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;


entity custominstruction is
port(
	signal clk: in std_logic;				-- CPU system clock (always required)
	signal reset: in std_logic;				-- CPU master asynchronous active high reset (always required)
	signal clk_en: in std_logic;				-- Clock-qualifier (always required)
	--signal start: in std_logic;				-- Active high signal used to specify that inputs are valid (always required)
	--signal done: out std_logic;				-- Active high signal used to notify the CPU that result is valid (required for variable multi-cycle)
	signal n: in std_logic_vector(5 downto 0);		-- N-field selector (always required), modify width to match the number of unique operations within the custom instruction
	signal dataa: in std_logic_vector(31 downto 0);		-- Operand A (always required)
	signal datab: in std_logic_vector(31 downto 0);		-- Operand B (optional)
	signal result: out std_logic_vector(31 downto 0)	-- result (always required)
);
end entity custominstruction;



architecture a_custominstruction of custominstruction is

	-- local custom instruction signals
	signal I0: std_logic_vector(15 downto 0):= (others => '0');
	signal I1: std_logic_vector(15 downto 0):= (others => '0');
	signal I2: std_logic_vector(15 downto 0):= (others => '0');
	signal I3: std_logic_vector(15 downto 0):= (others => '0');
	signal I4: std_logic_vector(15 downto 0):= (others => '0');
	signal I5: std_logic_vector(15 downto 0):= (others => '0');
	signal I6: std_logic_vector(15 downto 0):= (others => '0');
	signal I7: std_logic_vector(15 downto 0):= (others => '0');
	
		
	signal r_I0: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I1: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I2: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I3: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I4: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I5: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I6: std_logic_vector(15 downto 0):= (others => '0');
	signal r_I7: std_logic_vector(15 downto 0):= (others => '0');
	
	
	signal O0: std_logic_vector(15 downto 0);
	signal O1: std_logic_vector(15 downto 0);
	signal O2: std_logic_vector(15 downto 0);
	signal O3: std_logic_vector(15 downto 0);
	signal O4: std_logic_vector(15 downto 0);
	signal O5: std_logic_vector(15 downto 0);
	signal O6: std_logic_vector(15 downto 0);
	signal O7: std_logic_vector(15 downto 0);
	
	type entries is array (63 downto 0) of std_logic_vector(15 downto 0); 
	signal input : entries;
	signal intermediate : entries;
	signal final : entries;
	signal control : std_logic_vector(3 downto 0); 
	signal phase_one_counter: std_logic_vector(4 downto 0); 
	signal phase_two_counter: integer; 

	
function reverse_any_vector (a: in std_logic_vector)
return std_logic_vector is
  variable result: std_logic_vector(a'RANGE);
  alias aa: std_logic_vector(a'REVERSE_RANGE) is a;
begin
  for i in aa'RANGE loop
    result(i) := aa(i);
  end loop;
  return result;
end; -- function reverse_any_vector
	
begin

idct_1D : entity work.idct_1D 
    port map (
		pass  => not(n(4)),

		i0 => I0,
		i1 => I1,
		i2 => I2,
		i3 => I3,
		i4 => I4,
		i5 => I5,
		i6 => I6,
		i7 => I7,

		o0 => O0,
		o1 => O1,
		o2 => O2,
		o3 => O3,
		o4 => O4,
		o5 => O5,
		o6 => O6,
		o7 => O7
    );
	-- custom instruction logic (note:  external interfaces can be used as well)

	-- use the n[7..0] port as a select signal on a multiplexer to select the value to feed result[31..0]

	
	control <= n(3 downto 0); 
	
	--control varuables
	process 
	begin 
		wait until rising_edge(clk);
		if clk_en = '1' then
			if control = "0001" then 
				phase_one_counter <= std_logic_vector(unsigned(phase_one_counter) + 1); 
				phase_two_counter <= 0; 
			elsif  control = "0010" then 
				phase_one_counter <= "00000";  
				phase_two_counter <= phase_two_counter + 1; 
			else 
				phase_one_counter <= "00000";
				phase_two_counter <= 0; 
			end if; 
		end if; 
	end process; 

		
	process(control, phase_two_counter)
	begin
		if control = "0010" then
			if(phase_two_counter >= 2) then 
				result <= final((phase_two_counter-8)*2) & final(((phase_two_counter-8)*2) + 1);
			else
				result <= (others => '0');
			end if;
		end if; 
	end process;

	process
	begin
		wait until rising_edge(clk);
		if clk_en = '1' then
			if control = "0001" then --hard coded pipelined first pass
				if(phase_one_counter = "00000") then 
					input(0) <= dataa(31 downto 16);
					input(1) <= dataa(15 downto 0);
					input(2) <= datab(31 downto 16);
					input(3) <= datab(15 downto 0);
				elsif (phase_one_counter < "10001") then 
					if (phase_one_counter(0) = '1') then
						I0 <= input(0);
						I1 <= input(1);
						I2 <= input(2);
						I3 <= input(3);
						I4 <= dataa(31 downto 16);
						I5 <= dataa(15 downto 0);
						I6 <= datab(31 downto 16);
						I7 <= datab(15 downto 0);
					else
						input(0) <= dataa(31 downto 16);
						input(1) <= dataa(15 downto 0);
						input(2) <= datab(31 downto 16);
						input(3) <= datab(15 downto 0);
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 0) <= O0;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 1) <= O1;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 2) <= O2;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 3) <= O3;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 4) <= O4;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 5) <= O5;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 6) <= O6;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 7) <= O7;
					end if;
				else -- phase 1 counter is 16
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 0) <= O0;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 1) <= O1;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 2) <= O2;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 3) <= O3;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 4) <= O4;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 5) <= O5;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 6) <= O6;
						intermediate((to_integer(unsigned(phase_one_counter) srl 1)-1)*8 + 7) <= O7;
				end if; 
			elsif control = "0010" then -- hard coded pipelined second pass 
				if (phase_two_counter = 0) then 
					I0 <= intermediate(0*8 + phase_two_counter);
					I1 <= intermediate(1*8 + phase_two_counter);
					I2 <= intermediate(2*8 + phase_two_counter);
					I3 <= intermediate(3*8 + phase_two_counter);
					I4 <= intermediate(4*8 + phase_two_counter);
					I5 <= intermediate(5*8 + phase_two_counter);
					I6 <= intermediate(6*8 + phase_two_counter);
					I7 <= intermediate(7*8 + phase_two_counter);
				elsif (phase_two_counter < 8) then
					I0 <= intermediate(0*8 + phase_two_counter);
					I1 <= intermediate(1*8 + phase_two_counter);
					I2 <= intermediate(2*8 + phase_two_counter);
					I3 <= intermediate(3*8 + phase_two_counter);
					I4 <= intermediate(4*8 + phase_two_counter);
					I5 <= intermediate(5*8 + phase_two_counter);
					I6 <= intermediate(6*8 + phase_two_counter);
					I7 <= intermediate(7*8 + phase_two_counter);
					final(0*8 + phase_two_counter-1) <= O0;
					final(1*8 + phase_two_counter-1) <= O1;
					final(2*8 + phase_two_counter-1) <= O2;
					final(3*8 + phase_two_counter-1) <= O3;
					final(4*8 + phase_two_counter-1) <= O4;
					final(5*8 + phase_two_counter-1) <= O5;
					final(6*8 + phase_two_counter-1) <= O6;
					final(7*8 + phase_two_counter-1) <= O7;
				elsif(phase_two_counter = 8) then
					final(0*8 + phase_two_counter-1) <= O0;
					final(1*8 + phase_two_counter-1) <= O1;
					final(2*8 + phase_two_counter-1) <= O2;
					final(3*8 + phase_two_counter-1) <= O3;
					final(4*8 + phase_two_counter-1) <= O4;
					final(5*8 + phase_two_counter-1) <= O5;
					final(6*8 + phase_two_counter-1) <= O6;
					final(7*8 + phase_two_counter-1) <= O7;
				end if;		
			else
				null; 
			end if;
		end if;
	end process;	
	

end architecture a_custominstruction;


