module lossless_decode(
	clk,
	reset,
	bitbuffer,
    AC,
    
    //outputs
	allout
);

//inputs
input clk,reset;
input [31:0] bitbuffer;
input [0:31] AC;	

// outputs
output reg[31:0] allout;

// local variables
reg [0:7] size;
reg [31:0] buff;
reg [0:7] bits;
reg [0:7] runlength;
reg [0:15] e;


always@(posedge clk)
begin
	//allout = HUFF_EXTEND(INPUT_BITS(bitbuffer,AC),AC);
	allout = 0;
	buff = bitbuffer;
	
	if(AC == 1) begin
		runlength = INPUT_BITS(buff,4);
		buff = buff << 4;
		size  = INPUT_BITS(buff,4);
		//temp3 = size;
		if(size == 0) begin
			//return a value of 0. Notice this is ok size if size == 0, then it's either a END or ZRL,
			//and if size > 0, the amplitude cannot be 0.
			e = 0;
			bits = 8;
		end else begin
			buff = buff << 4;
			e = HUFF_EXTEND(INPUT_BITS(buff,size),size);
			bits = size + 8;
		end
	end else begin
		runlength = 4;
		size = INPUT_BITS(buff,4);
		//temp3 = size;
		if(size == 0) begin
			e = 0;
			bits = 4;
		end else begin
			buff = buff << 4;
			e = HUFF_EXTEND(INPUT_BITS(buff,size),size);
			bits = size + 4;
		end
	end

	allout[31:24] = runlength;
	allout[23:8] = e;
	allout[7:0] = bits;
	
	
end


//#define HUFF_EXTEND(x,s)  
function [0:15] HUFF_EXTEND;
    input [0:7] x;
    input [0:7] s;
    begin    
        HUFF_EXTEND = ((x) < (1<<((s)-1)) ? (x) + (((-1)<<(s)) + 1) : (x));
    end
endfunction  

//extract num bits from the buffer and returns them
// #define INPUT_BITS(buffer, num) (buffer) >> (32 - (num))
function [0:7] INPUT_BITS;
    input [0:31] buffer;
    input [0:7] num;
    begin
		INPUT_BITS = (buffer) >> (32 - (num));
    end
endfunction

endmodule
