//
//  mjpeg423_decoder.c
//  mjpeg423app
//
//  Created by Rodolfo Pellizzoni on 12/24/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#include "mjpeg423_decoder.h"
//declaration. Function implemented in libnsbmp
//void encode_bmp(rgb_pixel_t* rgbblock, uint32_t w_size, uint32_t h_size, const char* filename);

volatile iframe_trailer_t* trailer;
//volatile int g_sector;
//volatile int g_offset;
volatile int key_edge_count = 0;
volatile int pause = 0;
volatile int fw_skip = 0;
volatile int bw_skip = 0;
volatile uint32_t num_iframes;
volatile int frame_index = 0;
volatile int enable_skip = 0; //This is the variable that enables skip. It is used in the edge case where the user tries to skip before starting the video
volatile char file_names[3][20] = {"v3fps.mpg", "vlong.mpg", "vshort.mpg"};
volatile int active_file = 0;
volatile int file_change = 0;

static void button_ISR(void * content, alt_u32 id)
{
	uint8_t key_edge;

	if(key_edge_count == 0)
	{
		key_edge_count = 1;
	}
	else
	{
		key_edge_count = 0;
		key_edge = IORD(PIO_0_BASE,3);
		switch(key_edge)
		{
			case 0x1:
				pause = pause?0:1;
				printf("INTERRUPT PAUSE: %d\n",pause);
			break;
			case 0x2:
				file_change = 1;
				break;
			case 0x4:
				//We can only enable fw skipping if bw_skip is not already enabled and we didnt pass the last I frame
				fw_skip = 1  & (bw_skip != 1) & (frame_index < trailer[num_iframes - 1].frame_index);
				break;
			case 0x8:
				bw_skip = 1  & (fw_skip != 1);
				break;
			default:
				break;
		}
	}

	IOWR(PIO_0_BASE, 3, 0x0);
}

void skip_forward(int * g_sector, int * g_offset, int  * cur_frame)
{
	int found_i_index = num_iframes - 1;;
	int dest_frame = *cur_frame + 30;
	int i;


	printf("curframe %d\n",*cur_frame);
	for(i = 0; i < num_iframes; i++)
	{
		if(dest_frame >= trailer[i].frame_index)
		{
			found_i_index = i;
		}
		else
		{
			break;
		}
	}
	printf("found i index %d\n",found_i_index);
	(*g_sector) = ceil((trailer[found_i_index].frame_position)/512.0) - 1;
	(*g_offset) = (trailer[found_i_index].frame_position)%512;
	(*cur_frame) = trailer[found_i_index].frame_index;
}

void skip_backward(int * g_sector, int * g_offset, int  * cur_frame)
{
	int found_i_index = 0;
	int dest_frame = *cur_frame - 30;
	int i;

	printf("curframe %d\n",*cur_frame);
	for(i = num_iframes - 1; i >= 0; i--)
	{
		if(dest_frame >= (int)(trailer[i].frame_index))
		{
			found_i_index = i;
			break;
		}
	}
	printf("found i index %d\n",found_i_index);
	(*g_sector) = ceil((trailer[found_i_index].frame_position)/512.0) - 1;
	(*g_offset) = (trailer[found_i_index].frame_position)%512;
	(*cur_frame) = trailer[found_i_index].frame_index;
}



short int OPEN_SD_CARD(const char * filename_in, int * sector_num)
{
	//initialize device
	alt_up_sd_card_dev *device_reference = alt_up_sd_card_open_dev(
			ALTERA_UP_SD_CARD_AVALON_INTERFACE_0_NAME);
	if (device_reference == NULL) {
		printf("Cannot open SD Device\n");
		return NULL;
	}
	if (!alt_up_sd_card_is_Present()) {
		printf("There is no SD card in the slot\n");
		return NULL;
	}
	if (!alt_up_sd_card_is_FAT16()) {
		printf("SD card is not of FAT16 type\n");
		return NULL;
	}

	//open file v3fps.mpg
	short int file_handle = alt_up_sd_card_fopen(filename_in, 0);
	if (file_handle == -1) {
		printf("Cannot find file\n");
		return NULL;
	}

	//create list of sectors
	if (!sd_card_create_sectors_list(file_handle)) {
		printf("Cannot create sectors list\n");
		return NULL;
	}

	//create filebuffer to hold entire file
	printf("File size %d\n", sd_card_file_size(file_handle));

	//uint8_t* filebuffer;
	*sector_num = ceil(sd_card_file_size(file_handle) / 512.0);

	return file_handle;
}


void get_trailer_buffer_start(uint32_t payload_size)
{
	//This is the offset to reach the top of the payload.
	int i,j;
	int start_sec = ceil((5*sizeof(uint32_t) + payload_size)/512.0)-1;
	int buffer_size =2*sizeof(uint32_t)*num_iframes;
	//printf("buffer size = %d\n", buffer_size);
	int end_sec = ceil((5*sizeof(uint32_t) + payload_size + buffer_size)/512.0)-1;
	//printf("File size (trailer): %d\n", 5*sizeof(uint32_t) + payload_size + buffer_size);

	int t_bytes = 0;
	uint8_t * pos = malloc(buffer_size);
	//printf("Allocation: %x\n", pos);
	uint8_t * start_pos = pos;
	int offset;

	//printf("start_sec %d\n", start_sec);
	//printf("end_sec %d\n", end_sec);

	int read_flag = 0;

	int count = 0;
	for(i = start_sec; i <= end_sec; i++ )
	{
		//If this is the first sector then we are going to start somewhere inside the sector so lets get

		if(i == start_sec)
		{
			offset =  (5*sizeof(uint32_t) + payload_size)%512 ;
		}
		else
		{
			//Otherwise we start at the start of the sector
			offset = 0;
		}
		//printf("offset: %d\n",offset);
		//Read the sector in question
		sd_card_start_read_sector(i);
		//Wait for sector read to finish
		if (!sd_card_wait_read_sector()) {
			printf("Cannot read %d-th sector\n", i);
			return NULL;
		}

		//starting from the offset read all of the trailer data
		for (j = offset; (j < 512) && (t_bytes < buffer_size) ; j += 4,t_bytes += 4) {
			uint32_t temp = IORD_32DIRECT(buffer_memory, j);

			if(!read_flag)
			{
				trailer[count].frame_index = temp;
				//printf("%d | %d | %d | INDEX %d\n", i, j, temp, trailer[count].frame_index);
				read_flag = 1;
			}
			else
			{
				trailer[count].frame_position = temp;
				//printf("%d | %d | %d | POS %d\n", i, j, temp, trailer[count].frame_position);
				read_flag = 0;
				count++;
			}
		}
	}
}



uint8_t * get_header_buffer()
{
	int j;
	uint8_t * pos;

	//Allocate 20 bytes for header
	if ((pos = malloc(5*sizeof(uint32_t))) == NULL) {
		printf("Cannot allocate  header filebuffer\n");
		return NULL;
	}

	//Read the first sector
	sd_card_start_read_sector(0);

	//Wait for sector read to finish
	if (!sd_card_wait_read_sector()) {
		printf("Cannot read %d-th sector\n", 0);
		return NULL;
	}

	//write the filebuffer;
	for (j = 0; j < 20; j += 4) {
		*((uint32_t*) (pos + j)) = IORD_32DIRECT(buffer_memory, j);
	}

	return pos;
}





void get_frame_data(uint8_t ** buffer, int size, int * g_sector, int * g_offset )
{
	int i,j;
	int ref_offset = (*g_offset);
	int offset, start_sec, end_sec;
	int count = 0;
	//(*buffer) = malloc(size);
	uint8_t * pos = (*buffer);
	start_sec = (*g_sector);
	end_sec = start_sec + ceil((ref_offset+size)/512.0) - 1;


	for(i = start_sec; i <= end_sec; i++ )
	{
		if(i == start_sec)
		{
			offset = ref_offset;
		}
		else
		{
			//Otherwise we start at the start of the sector
			offset = 0;
		}

		//Read the sector in question
		sd_card_start_read_sector(i);
		//Wait for sector read to finish
		if (!sd_card_wait_read_sector()) {
			printf("Cannot read %d-th sector\n", i);
			return NULL;
		}

		//starting from the offset read all of the trailer data
		for (j = offset; (j < 512) && (count < size) ; j += 4,count += 4) {
			*((uint32_t*) (pos + count)) = IORD_32DIRECT(buffer_memory, j);
		}
	}
	//Update the global variables
	(*g_sector) = end_sec;
	(*g_offset) = j;
}

void mjpeg423_decode_init()
{
	int sector_num;
	alt_irq_register(PIO_0_IRQ, (void*)0, button_ISR);
	IOWR(PIO_0_BASE, 3, 0x0);
	IOWR(PIO_0_BASE, 2, 0xf);

	while(pause);


	short int file_handle;
	while(1)
	{
		printf("\n\nNOW PLAYING: ");
		printf(file_names[active_file]);
		printf("\n");
		file_handle = OPEN_SD_CARD(file_names[active_file], &sector_num);
		mjpeg423_decode(sector_num);
		alt_up_sd_card_fclose(file_handle);
	}
}


//main decoder function
void mjpeg423_decode(int sector_num)
{
	//Get the inerrupts going
    //header and payload info
    uint32_t num_frames, w_size, h_size, payload_size;
    uint32_t Ysize, Cbsize, frame_size, frame_type;

    //printf("Total Sectors %d\n", sector_num);
    uint8_t * filebuffer = get_header_buffer();
    uint8_t * f_cur_pos = filebuffer;

    if(filebuffer == NULL)
    {
    	printf("ERROR: FAILED TO READ SD CARD\n");
    	return;
    }

    f_cur_pos = filebuffer;
    
    //read header
	num_frames = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
   
	w_size = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
   
    h_size = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
   
    num_iframes = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
    
    payload_size = *((uint32_t *)f_cur_pos); f_cur_pos += 4;

    int hCb_size = h_size/8;           //number of chrominance blocks
    int wCb_size = w_size/8;
    int hYb_size = h_size/8;           //number of luminance blocks. Same as chrominance in the sample app
    int wYb_size = w_size/8;
    
    //trailer structure
    trailer = malloc(sizeof(iframe_trailer_t)*num_iframes);
    
    //main data structures. See lab manual for explanation
    rgb_pixel_t* rgbblock;
    if((rgbblock = malloc(w_size*h_size*sizeof(rgb_pixel_t)))==NULL) error_and_exit("cannot allocate rgbblock");
    color_block_t* Yblock;
    if((Yblock = malloc(hYb_size * wYb_size * 64))==NULL) error_and_exit("cannot allocate Yblock");
    color_block_t* Cbblock;
    if((Cbblock = malloc(hCb_size * wCb_size * 64))==NULL) error_and_exit("cannot allocate Cbblock");
    color_block_t* Crblock;
    if((Crblock = malloc(hCb_size * wCb_size * 64))==NULL) error_and_exit("cannot allocate Crblock");;
    dct_block_t* YDCAC;
    if((YDCAC = malloc(hYb_size * wYb_size * 64 * sizeof(DCTELEM)))==NULL) error_and_exit("cannot allocate YDCAC");
    dct_block_t* CbDCAC;
    if((CbDCAC = malloc(hCb_size * wCb_size * 64 * sizeof(DCTELEM)))==NULL) error_and_exit("cannot allocate CbDCAC");
    dct_block_t* CrDCAC;
    if((CrDCAC = malloc(hCb_size * wCb_size * 64 * sizeof(DCTELEM)))==NULL) error_and_exit("cannot allocate CrDCAC");
    //Ybitstream is assigned a size sufficient to hold all bistreams
    //the bitstream is then read from the file into Ybitstream
    //the remaining pointers simply point to the beginning of the Cb and Cr streams within Ybitstream
    uint8_t* Ybitstream;
    if((Ybitstream = malloc(hYb_size * wYb_size * 64 * sizeof(DCTELEM) + 2 * hCb_size * wCb_size * 64 * sizeof(DCTELEM)))==NULL) error_and_exit("cannot allocate bitstream");
    uint8_t* Cbbitstream;
    uint8_t* Crbitstream;
    
    //read trailer. Note: the trailer information is not used in the sample decoder app
    //set file to beginning of trailer
    free(filebuffer);

    get_trailer_buffer_start(payload_size);
   
    //set it back to beginning of payload
    alt_up_pixel_buffer_dma_dev* p_buffer = alt_up_pixel_buffer_dma_open_dev("/dev/video_pixel_buffer_dma_0");
    alt_up_pixel_buffer_dma_clear_screen(p_buffer,0);
    alt_up_pixel_buffer_dma_clear_screen(p_buffer,1);

    //Set the globals


    int g_sector = 0;
    int g_offset = 20;

    uint8_t * frame_header = malloc(sizeof(int)*16);
    int last_i_index = num_iframes - 1;
	pause = 1;
	
	for(frame_index = 0; frame_index < num_frames; frame_index++){
    	//printf("\nPAUSE: %d",pause);
    	while(alt_up_pixel_buffer_dma_check_swap_buffers_status(p_buffer));
    	while(pause)
    	{
    		if(fw_skip || bw_skip || file_change)
    		{
				break;
    		}
    	}
		if(file_change)
		{
			file_change = 0;
			active_file  = (active_file+1)%3;
			break;
		}

    	if(fw_skip)
    	{
    		skip_forward(&g_sector, &g_offset, &frame_index);
    		fw_skip = 0;
    	}
    	else if(bw_skip)
    	{
    		skip_backward(&g_sector, &g_offset, &frame_index);
    		bw_skip = 0;
    	}
 
    	get_frame_data(&frame_header, 16, &g_sector, &g_offset );
    	f_cur_pos = frame_header;
		frame_size = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
        frame_type = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
        Ysize = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
        Cbsize = *((uint32_t *)f_cur_pos); f_cur_pos += 4;
        get_frame_data(&Ybitstream, frame_size - 16, &g_sector, &g_offset);
		//memcpy(Ybitstream, frame_payload, frame_size - 16);

        //set the Cb and Cr bitstreams to point to the right location
        Cbbitstream = Ybitstream + Ysize;
        Crbitstream = Cbbitstream + Cbsize;
        
        //lossless decoding
        lossless_decode(hYb_size*wYb_size, Ybitstream, YDCAC, Yquant, frame_type);
        lossless_decode(hCb_size*wCb_size, Cbbitstream, CbDCAC, Cquant, frame_type);
        lossless_decode(hCb_size*wCb_size, Crbitstream, CrDCAC, Cquant, frame_type);
        
		uint32_t result;
		uint32_t a = 134;
		uint32_t b = 134;

		for(int b = 0; b < hYb_size*wYb_size; b++) idct(YDCAC[b], Yblock[b]);
        for(int b = 0; b < hCb_size*wCb_size; b++) idct(CbDCAC[b], Cbblock[b]);
        for(int b = 0; b < hCb_size*wCb_size; b++) idct(CrDCAC[b], Crblock[b]);
        
		for(int b = 0; b < hCb_size*wCb_size; b++) ycbcr_to_rgb(b/wCb_size*8, b%wCb_size*8, w_size, Yblock[b], Cbblock[b], Crblock[b], p_buffer);
        alt_up_pixel_buffer_dma_swap_buffers(p_buffer);

    } //end frame iteration
    
    printf("Number of Frames: %d\n",num_frames);

    free(rgbblock); 
    free(Yblock);
    free(Cbblock);
    free(Crblock);
    free(YDCAC);
    free(CbDCAC);
    free(CrDCAC);
    free(Ybitstream);
    free(frame_header);
}