//
//  idct.c
//  mjpeg423app
//
//  Created by Rodolfo Pellizzoni on 12/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include "mjpeg423_decoder.h"
#include "../common/dct_math.h"
#include "../common/util.h"


#ifndef NULL_DCT

/*
* This implementation is based on an algorithm described in
*   C. Loeffler, A. Ligtenberg and G. Moschytz, "Practical Fast 1-D DCT
*   Algorithms with 11 Multiplications", Proc. Int'l. Conf. on Acoustics,
*   Speech, and Signal Processing 1989 (ICASSP '89), pp. 988-991.
* The primary algorithm described there uses 11 multiplies and 29 adds.
* We use their alternate method with 12 multiplies and 32 adds.
* The advantage is that no data path contains more than one multiplication.
*/

/* normalize the result between 0 and 255 */
/* this is required to handle precision errors that might cause the decoded result to fall out of range */
#define NORMALIZE(x) (temp = (x), ( (temp < 0) ? 0 : ( (temp > 255) ? 255 : temp  ) ) )
#define BYTE 8


void idct(pdct_block_t DCAC, pcolor_block_t block)
{
	/*
	 *
	int32_t tmp0, tmp1, tmp2, tmp3;
    int32_t tmp10, tmp11, tmp12, tmp13;
    int32_t z1, z2, z3, z4, z5;
    int32_t temp;
    */
	volatile uint32_t rez[4];

    DCTELEM* inptr;
    DCTELEM* row_ref;
    uint8_t* outptr;
    SHIFT_TEMPS

   /* printf("\n\nINPUT BLOCK\n");
    for(int i = 0; i < 8; i++)
    {
    	inptr = DCAC[i];
    	for(int j = 0; j < 8; j++)
    	{
    		//set.dataa = (inptr[0] << 16) | (inptr[1]);
    		//set.datab = (inptr[2] << 16) | (inptr[3]);
    		printf("%d ", inptr[j]);
    	}
    	printf("\n");
    }
    printf("\n");*/
    //set.dataa = (inptr[0] << 16) | (inptr[1] & 0xFFFF);
    //set.datab = (inptr[2] << 16) | (inptr[3] & 0xFFFF);
	//set.dataa = (inptr[4] << 16) | (inptr[5] & 0xFFFF);
	//set.datab = (inptr[6] << 16) | (inptr[7] & 0xFFFF) ;

	ALT_CI_CUSTOMINSTRUCTION_INST(48,0,0);
    uint32_t a;
    uint32_t b;
	//row 0
	for (int i=0; i < 8; i++) { //Phase_one_counter  1-16
	    //inptr = temp2[i];
		inptr = DCAC[i];
		row_ref = inptr;

		//printf("%04x %04x %04x %04x\n",inptr[0] & 0xFFFF,inptr[1] & 0xFFFF,inptr[2] & 0xFFFF,inptr[3] & 0xFFFF );
		a = *((uint32_t *)inptr);
		inptr+=2;
		b = *((uint32_t *)inptr);

		//printf("%08x %08x\n",a,b);
		inptr += 2;// &row_ref[4];
		ALT_CI_CUSTOMINSTRUCTION_INST(49,a,b);
		a = *((uint32_t *)inptr);
		inptr += 2;
		b = *((uint32_t *)inptr);
		//printf("%08x %08x\n",set.dataa,set.datab);
		ALT_CI_CUSTOMINSTRUCTION_INST(49,a,b);
	}

	// 1 of processing before we start getting outputs cycles before last stage

	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
	ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);


	// read output values
	for(int row = 0; row < 8; row++)
	{
		outptr = block[row];
		rez[0] = ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
		rez[1] = ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
		rez[2] = ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);
		rez[3] = ALT_CI_CUSTOMINSTRUCTION_INST(34,0,0);

		outptr[0] = rez[0] >> 16;
		outptr[1] = rez[0];
		outptr[2] = rez[1] >> 16;
		outptr[3] = rez[1];
		outptr[4] = rez[2] >> 16;
		outptr[5] = rez[2];
		outptr[6] = rez[3] >> 16;
		outptr[7] = rez[3];
	}


    /*printf("\n\nOUTPUT BLOCK\n");
    for(int i = 0; i < 8; i++)
     {
     	outptr = block[i];
     	for(int j = 0; j < 8; j++)
     	{
     		printf("%d ", outptr[j]);
     	}
     	printf("\n");
     }*/
}

#else /* Null implementation */

void idct(pdct_block_t DCAC, pcolor_block_t block)
{
    for(int row = 0 ; row < 8; row ++)
        for(int column = 0; column < 8; column++)
            block[row][column] = DCAC[row][column];
}

#endif

