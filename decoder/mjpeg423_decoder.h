//
//  mjpeg423_decoder.h
//  mjpeg423app
//
//  Created by Rodolfo Pellizzoni on 12/24/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#ifndef mjpeg423app_mjpeg423_decoder_h
#define mjpeg423app_mjpeg423_decoder_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common/mjpeg423_types.h"
#include "mjpeg423_decoder.h"
#include "../common/util.h"
#include "../sd_card.h"
#include "io.h"
#include "altera_up_avalon_video_pixel_buffer_dma.h"

void mjpeg423_decode();
void mjpeg423_decode_init();
void ycbcr_to_rgb(int h, int w, uint32_t w_size, pcolor_block_t Y, pcolor_block_t Cb, pcolor_block_t Cr, alt_up_pixel_buffer_dma_dev* pixel_buffer);
void idct(dct_block_t DCAC, color_block_t block);
void lossless_decode(int num_blocks, void* bitstream, dct_block_t* DCACq, dct_block_t quant, BOOL P);
uint8_t * read_SD_CARD();


struct custom_bits{
	unsigned int value : 6;
}n;

struct set{
	uint32_t dataa,datab;
}set,set0,set1;




#endif
